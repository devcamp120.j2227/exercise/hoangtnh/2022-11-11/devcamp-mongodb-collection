// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware
const courseMiddleware = require("../middlewares/courseMiddleware");

router.get("/courses", courseMiddleware.getAllCourseMiddleware, (request, response) => {
    response.json({
        message: "Get ALL Course"
    })
})

router.post("/courses", courseMiddleware.createCourseMiddleware, (request, response) => {
    response.json({
        message: "Create Course"
    })
})

router.get("/courses/:courseId", courseMiddleware.getDetailCourseMiddleware, (request, response) => {
    const courseId = request.params.courseId;

    response.json({
        message: "Get Course ID = " + courseId
    })
})

router.put("/courses/:courseId", courseMiddleware.updateCourseMiddleware, (request, response) => {
    const courseId = request.params.courseId;

    response.json({
        message: "Update Course ID = " + courseId
    })
})

router.delete("/courses/:courseId", courseMiddleware.deleteCourseMiddleware, (request, response) => {
    const courseId = request.params.courseId;

    response.json({
        message: "Delete Course ID = " + courseId
    })
})

module.exports = router;